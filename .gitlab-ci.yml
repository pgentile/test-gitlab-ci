# Gitlab CI reference for Maven: https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/Maven.gitlab-ci.yml
include:
  - template: Security/SAST.gitlab-ci.yml

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  BUILD_IMAGE_TAG: "1"
  BUILD_IMAGE_BASENAME: $CI_REGISTRY_IMAGE/build
  BUILD_IMAGE: $CI_REGISTRY_IMAGE/build:$BUILD_IMAGE_TAG

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG =~ /^v/
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_BRANCH =~ /^release-/
    - if: '$FORCE == "true"'

cache:
  policy: pull

stages:
  - build-image
  - validate
  - set-feature-version
  - prepare
  - build
  - test
  - release
  - i18n
  - pages
  - deploy

default:
  image:
    name: $BUILD_IMAGE
  before_script:
    - export

.docker:
  image:
    name: "docker:20.10.9"
  services:
    - docker:20.10.9-dind
  cache: {}
  before_script:
    - export
    - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - 'if [[ "$DEBUG" == true ]]; then docker info; fi'
  variables:
    DOCKER_BUILDKIT: "1"

build-image:
  extends: .docker
  stage: build-image
  resource_group: $BUILD_IMAGE
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
    - docker pull $BUILD_IMAGE || true
    - 'docker build -t $BUILD_IMAGE --pull --cache-from $BUILD_IMAGE --build-arg BUILDKIT_INLINE_CACHE=1 - <build.Dockerfile'
    - docker push $BUILD_IMAGE

validate:
  stage: validate
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $FORCE
  cache: {}
  script:
    - 'if [[ "$DEBUG" == true ]]; then yarn config list; fi'
    - yarn
    - yarn lint
    - yarn danger ci --failOnErrors

set-feature-version:
  stage: set-feature-version
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^renovate/
      when: never
    - if: $CI_MERGE_REQUEST_IID
  cache: {}
  script:
    - yarn install --production
    - './version.js feature $CI_COMMIT_REF_SLUG --snapshot'
  artifacts:
    paths:
      - "pom.xml"
      - "**/pom.xml"
      - "package.json"

get-version:
  stage: prepare
  rules:
    - when: on_success
  cache: {}
  script:
    - yarn install --production
    - echo "VERSION=$(./version.js get)" >version.env
  artifacts:
    reports:
      dotenv:
        - version.env

go-offline:
  stage: prepare
  rules:
    - if: $CI_COMMIT_REF_NAME !~ /^renovate/
  cache:
    policy: pull-push
    paths:
      - .m2/repository
  script:
    - 'if [[ "$DEBUG" == true ]]; then mvn help:effective-settings; fi'
    - mvn dependency:go-offline

build:
  stage: build
  rules:
    - if: $CI_COMMIT_REF_NAME !~ /^renovate/
  script:
    - 'if [[ "$DEBUG" == true ]]; then mvn help:effective-settings; fi'
    - mvn $MAVEN_PHASE
  artifacts:
    reports:
      junit:
        - "**/target/surefire-reports/TEST-*.xml"
  variables:
    MAVEN_PHASE: deploy

build-for-renovate:
  extends: build
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^renovate/
  variables:
    MAVEN_PHASE: verify

tag-build-image:
  extends: .docker
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - docker pull $BUILD_IMAGE
    - docker tag $BUILD_IMAGE $BUILD_IMAGE_BASENAME:release-$VERSION
    - docker push $BUILD_IMAGE_BASENAME:release-$VERSION

release:
  stage: release
  needs:
    - build
  rules:
    - if: $RELEASE_TYPE
    - if: $CI_COMMIT_BRANCH
      when: manual
  script:
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts
    - eval $(ssh-agent -s)
    - 'chmod 600 $SSH_PRIVATE_KEY && ssh-add $SSH_PRIVATE_KEY'
    - 'GIT_REMOTE_URL=$(echo $CI_REPOSITORY_URL | sed -e "s/^.*@/git@/" | sed "s#/#:#")'
    - git remote set-url origin $GIT_REMOTE_URL
    - '[[ -n "$RELEASE_TYPE" ]] || (echo "RELEASE_TYPE not defined" >&2 && exit 1)'
    - yarn install --production
    - './release.sh $RELEASE_TYPE'

i18n:
  stage: i18n
  needs: []
  rules:
    - changes:
        - src/main/resources/messages/fr.json
      when: manual
    - if: $FORCE
      when: manual
  script: "echo Pushing translation files for branch $CI_COMMIT_REF_NAME"

deploy:
  stage: deploy
  needs:
    - build
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  trigger:
    include:
      - local: .gitlab-ci.deploy.yml

pages:
  stage: pages
  needs: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image:
    name: bash:latest
  cache: {}
  script: echo Publishing pages...
  artifacts:
    paths:
      - public
