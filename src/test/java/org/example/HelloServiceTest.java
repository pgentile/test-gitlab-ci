package org.example;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class HelloServiceTest {

    @Test
    void it_should_say_hello() {
        final var service = new HelloService();
        final var name = "Jean";
        final var result = service.sayHello(name);

        assertThat(result).startsWith("Hello").contains(name);
    }

}
