Tests avec GitLab CI
====================

Le but est de faire des tests avec les fichiers de config YAML de GitLab CI.

Livrer le projet
----------------

Créer une version avec le script de release :

```bash
./release.sh VERSION
```

Le script de release s'occupe de mettre à jour les POM, créer un tag, puis pousser
ce tag vers Gitlab. La pipeline prend ensuite la main pour générer les livrables.

Informations complémentaires
----------------------------

* Danger est utilisé pour contrôler le contenu des MR
