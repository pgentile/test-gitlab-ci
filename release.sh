#!/usr/bin/env bash

set -e

release_type="$1"

[[ -n "$release_type" ]] || (echo "ERROR: No release type defined" >&2; exit 1)

echo "Configure Git"
echo

if [[ -n "$GITLAB_USER_EMAIL" ]]; then
  git config user.email "$GITLAB_USER_EMAIL"
fi

if [[ -n "$GITLAB_USER_NAME" ]]; then
  git config user.name "$GITLAB_USER_NAME"
fi

version=$(./version.js get)
tag="v$version"
release_ref=$(git rev-parse HEAD)

echo "Release $version depuis la branche $CI_COMMIT_BRANCH"
echo "Le tag $tag sera créé"
echo

echo "Préparer la version suivante"
echo

./version.js "$release_type" --snapshot
new_version=$(./version.js get)

echo "Prochaine version : $new_version"

git add -u
git commit --no-verify -m "[NEXT RELEASE] Initialized next version $new_version"

next_ref=$(git rev-parse HEAD)
git push --no-verify origin "$next_ref:$CI_COMMIT_BRANCH"

git checkout "$release_ref"

echo "Tag $tag à partir du commit $release_ref"
echo

./version.js set

git add -u
git commit --no-verify -m "[RELEASE] Version $version"
git tag "$tag"
git push --no-verify origin tag "$tag"

echo
echo "Et voilà, la release $version est terminée. Encore une victoire de canard ! 🦆"
