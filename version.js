#!/usr/bin/env node

const { promisify } = require("util");
const { exec: execCallback } = require("child_process");
const { writeFile, readFile } = require("fs/promises");
const path = require("path");
const yargs = require("yargs/yargs");
const { hideBin } = require("yargs/helpers");
const { parse: parseSemVer } = require("semver");

const exec = promisify(execCallback);

(() => {
  yargs(hideBin(process.argv))
    .usage("Usage: $0 <command> --snapshot")
    .option("snapshot", { boolean: true, default: false })
    .command(
      "major",
      "Major version",
      () => {},
      (args) => updateVersion("major", { snapshot: args.snapshot })
    )
    .command(
      "minor",
      "Minor version",
      () => {},
      (args) => updateVersion("minor", { snapshot: args.snapshot })
    )
    .command(
      "patch",
      "Patch version",
      () => {},
      (args) => updateVersion("patch", { snapshot: args.snapshot })
    )
    .command(
      "feature <name>",
      "Generate a feature version",
      (builder) => builder.positional("name", { description: "Feature name" }),
      (args) =>
        updateVersion("feature", { snapshot: args.snapshot, name: args.name })
    )
    .command(
      "set",
      "Set the version",
      () => {},
      () => updateVersion("set")
    )
    .command(
      "get",
      "Read the current version",
      () => {},
      () => readVersion()
    )
    .help()
    .alias("h", "help")
    .demandCommand().argv;
})();

async function updateVersion(releaseType, { name, snapshot = false } = {}) {
  const package = await readPackageFile();

  const version = parseSemVer(package.version);

  switch (releaseType) {
    case "set":
      snapshot = false;
      break;
    case "feature":
      version.prerelease = ["feature", name];
      break;
    default:
      version.inc(releaseType);
      break;
  }

  const newVersion = version.format();

  console.info("New version is", newVersion);

  package.version = newVersion;

  console.info("Updading package.json");

  await writePackageFile(package);

  console.info("Updating pom.xml");

  const pomVersion = snapshot ? newVersion + "-SNAPSHOT" : newVersion;

  await exec(
    `mvn versions:set -DgenerateBackupPoms=false -DnewVersion="${pomVersion}"`,
    {
      cwd: __dirname,
    }
  );
}

async function readVersion() {
  const { version } = await readPackageFile();
  process.stdout.write(version + "\n");
}

async function readPackageFile() {
  const packageFile = path.join(__dirname, "package.json");
  const content = await readFile(packageFile, "utf8");
  return JSON.parse(content);
}

async function writePackageFile(package) {
  const packageFile = path.join(__dirname, "package.json");
  const serialized = JSON.stringify(package, undefined, 2);
  await writeFile(packageFile, serialized + "\n", "utf8");
}
